##### Vider sa `flask` dans une `lambda`

![PyConFr logo](doc/img/500-pycon-fr19.png)

_Novembre 2019 - Frédéric Zind - PyConFr_

+++

_Zappa_, ou déployer sur _AWS Lambda_ avec _Python_ & _Gitlab_

![python logo](doc/img/96-python.png)
![gitlab logo](doc/img/96-gitlab.png)
![aws logo](doc/img/96-jaws.png)
![knacss logo](doc/img/96-knacss.png)

Note:

- niveau débutant
- qui zappa ?
- qui pas Lambda ?
- qui pas utilisé AWS ?

---

### Moi

![TI-99](doc/img/ti99.jpg)

Note:

- Je m'appelle …
- études de mécanique
- gestion d'équipe, clients & matériels
- logistique & services
- libriste pratiquant depuis Debian 3 (woody/2002)
- 2018 veut devenir pro => Python

---

### Mon boulot

+++

![MF2i logo](doc/img/200-mf2i.png)

@ul
- TPE : infra & infogérance
- Arrivée en _mars 2019_
- **le seul développeur**
@ulend

---

### Avertissement

@fa[exclamation-triangle]

Ceci n'est **pas de recette miracle** : seulement un retour d'expérience avec **plus ou moins** de recul

---

### La genèse

@ul
- un test d'embauche
- la curiosité
- Gitlab-CI
- AWS difficilement contournable
@ulend

Note:

- déployer un algorithme d'Euclide (plus grand commun diviseur)
- serverless framework sympa mais complexe pour un débutant
- impatient de quitter quitter github
- AWS incontournable même si techno différente de mes valeurs

---

### Un _Hello World_ !

Note:

- Issu de la doc zappa

+++

![zappa Hello World 01](doc/img/zappa-hello_world-01.png)

Note:

- pip install zappa flask

+++

![zappa Hello World 02](doc/img/zappa-hello_world-02.png)

+++

![zappa Hello World 03](doc/img/zappa-hello_world-03.png)

Note:

- non-mentionné : un aws-credentials + AWS IAM

+++

![zappa Hello World 04](doc/img/zappa-hello_world-04.png)

+++

![zappa Hello World 05](doc/img/zappa-hello_world-05.png)

+++

![zappa Hello World 06](doc/img/zappa-hello_world-06.png)

+++

![zappa Hello World 07](doc/img/zappa-hello_world-07.png)

+++

![zappa Hello World 08](doc/img/zappa-hello_world-08.png)

Note:

- Non limité à Flask : compatbli WSGI
- Bonne doc

---

### Un peu plus !

… avec un projet `flask` mono-page utilisant les APIs _Wikipedia_ & _Mapbox_

---

![OC P7](doc/img/oc-dapy-p7.png)

Note:

- Qui connait OC ?
- Projet OC DAPy 7
- hébergé heroku & utilisant API `google maps`
- hors ligne à cause de l'API GOO qui veut un numéro de CB
- indulgence pour la qualité du code SVP

+++?code=doc/tree.txt&title=L'arborescence de l'app Flask

@snap[south]
@[2,16](De la config)
@snapend

@snap[south]
@[3](Génére la page d'accueil)
@snapend

@snap[south]
@[7-8,11](Du code…)
@snapend

@snap[south]
@[12-15,17-18](Du statiques)
@snapend

+++

Et ça fonctionne :

![GrandPy local](doc/img/grandpy-local-01.png)

+++

Et ça fonctionne :

![GrandPy local](doc/img/grandpy-local-02.png)

---

### Déploiement sur _les Internets_ !

![gitlab logo](doc/img/96-gitlab.png)

@fa[plus]

![aws logo](doc/img/96-jaws.png)

+++

#### (Gitlab ?)

![Gitlab Workflow](doc/img/700-gitlab-workflow-zappa.png)

Note:

- Qui ne connait pas?

+++?code=doc/tree.txt&title=L'arborescence de l'app Flask

@snap[south]
@[1](Config AWS)
@snapend

@snap[south]
@[5](Config de CI)
@snapend

@snap[south]
@[21](Config de Zappa)
@snapend

Note:

- Génère le fichier de conf AWS pour le runner gitlab
- Automatisation des fonctions zappa (update, depploy, etc)
- ici jobs PlantUML en plus
- app_function, aws_region, profile/project name, S3 bucket, etc.

+++

### Qu'est ce que ça donne?

![deploy diagram 01](https://free_zed.gitlab.io/grandpy/diagrams/deploy-01.png)

+++

![deploy diagram 02](https://free_zed.gitlab.io/grandpy/diagrams/deploy-02.png)

+++

![deploy diagram 03](https://free_zed.gitlab.io/grandpy/diagrams/deploy-03.png)

+++

![deploy diagram 04](https://free_zed.gitlab.io/grandpy/diagrams/deploy-04.png)

+++

@snap[north]
### Mini-bilan
@snapend

@snap[west]
Fastoche :
- Dev en local
- Code portable
- 100% python
@snapend

@snap[east]
Galère :
- Config AWS IAM
- facturation assymétrique
- sans doute d'autre choses
@snapend

---

@snap[north]
### Des questions ?
@snapend

@snap[west]
![QRcode](doc/img/qrcode-pro.zind.fr.png)
@snapend

@snap[south]
http://pro.zind.fr
@snapend

@snap[east]
### Merci !
@snapend

---

**Credits :**

- https://commons.wikimedia.org/wiki/File:TI99-IMG_1680.jpg by **Rama** [CC BY-SA 2.0 fr](https://creativecommons.org/licenses/by-sa/2.0/fr/deed.en)
- https://github.com/Miserlou/Zappa/
- https://openclassrooms.com/fr/projects/creez-grandpy-bot-le-papy-robot/assignment
- https://about.gitlab.com/product/continuous-integration/
