#!/usr/bin/env python3
# coding: utf8

"""
Author: [freezed](https://gitlab.com/free_zed) 10.03.2019
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of project [grandpy](https://gitlab.com/free_zed/grandpy/).

A HTML generator using jinja template
"""
from flask import render_template
import flask
from config import PREFERRED_URL_SCHEME, PROD_URL


app = flask.Flask(__name__)
app.config["SERVER_NAME"] = PROD_URL
app.config["PREFERRED_URL_SCHEME"] = PREFERRED_URL_SCHEME

with app.app_context():
    rendered = render_template("index.jinja")
    print(rendered)
