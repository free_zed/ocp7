#!/usr/bin/env python3
# coding: utf8

"""
Author: [freezed](https://gitlab.com/free_zed) 2018-08-23
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of project [grandpy](https://gitlab.com/free_zed/grandpy/).
"""
from os import environ

API_USED = "OSM"
GOOGLE_API_KEY = environ["GOO_API_KEY"]
MAPBOX_API_KEY = environ["MAPBOX_API_KEY"]
MAP_LINK = "https://opentopomap.org/#map=17/{lat}/{lon}"
# Useless if static URL will be generated as a relative
PROD_URL = "free_zed.gitlab.io/grandpy"
PREFERRED_URL_SCHEME = "https"
CORS_URL = "https://free_zed.gitlab.io"

# Used in OSM API to get feedback when requesting too much
EMAIL = environ["EMAIL_ADDRESS"]

# Set configuration for OSM & GOO API
API = {
    "MAP_SIZE": (600, 300),
    "MIN_QUERY_LEN": 5,
    # OSM API configuration
    "OSM": {
        "URL_GEO": "https://nominatim.openstreetmap.org/search",
        "QUERY_LABEL": "q",
        "GEO_PARAM": {"format": "json", "addressdetails": True},
        "RESPONSE_KEYS": {
            "city": [("address", "city"), ("address", "village"), ("address", "town")],
            "display_name": "display_name",
            "lat": "lat",
            "lon": "lon",
        },
        # MapBox API configuration
        "URL_MAP": "https://api.mapbox.com/v4/mapbox.streets/pin-s-marker+285A98({lon},{lat})/{lon},{lat},16/{h}x{w}.png",
    },
    # Google API configuration
    "GOO": {
        "URL_GEO": "https://maps.googleapis.com/maps/api/geocode/json",
        "QUERY_LABEL": "address",
        "GEO_PARAM": {"KEY": GOOGLE_API_KEY, "COUNTRY": "FR"},
        "RESPONSE_KEYS": {
            "city": ("truncated_address", "locality"),
            "display_name": "formatted_address",
            "lat": ("location", "lat"),
            "lon": ("location", "lng"),
        },
        "URL_MAP": "https://maps.googleapis.com/maps/api/staticmap",
    },
}
# Wikimedia API configuration
WIK_API = {
    "ROOT_URL": "https://fr.wikipedia.org/w/api.php",
    "CALL_PARAM": {"action": "query", "utf8": True, "format": "json", "list": "search"},
    "PARAM_EXTRAC": {
        "action": "query",
        "utf8": True,
        "format": "json",
        "prop": "extracts",
        "exlimit": 1,
        "explaintext": True,
        # 'exsentences':3,
        "exsectionformat": "plain",
        "exintro": True,
    },
}
