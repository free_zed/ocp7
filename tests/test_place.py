#!/usr/bin/env python3
# coding: utf8

"""
Author: [freezed](https://gitlab.com/free_zed) 2018-08-21
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of project [grandpy](https://gitlab.com/free_zed/grandpy/).
"""
import run as script
import models.place
import json
import urllib.parse as up


class TestPlace:
    TXTINPUT = "Salut GrandPy! Est-ce que tu connais l'adresse d'OpenClassrooms ?"
    PLACE = script.Place(TXTINPUT)
    OC_GEO_JSON = {
        "city": "Paris",
        "display_name": "OpenClassRooms, 7, Cité Paradis, Quartier de la Porte-Saint-Denis, Paris 10e Arrondissement, Paris, Île-de-France, France métropolitaine, 75010, France",
        "lat": "48.8747786",
        "lon": "2.3504885",
        "status": True,
    }

    def test_get_query(self):
        assert self.PLACE.query == self.TXTINPUT

    def test_geo_data_type(self):
        assert isinstance(self.PLACE.geo_data, dict)

    def test_article_data_type(self):
        assert isinstance(self.PLACE.article_data, dict)

    def test_compare(self):
        assert (
            self.PLACE.compare(self.TXTINPUT)
            == "salut grandpy! est ce que tu connais l adresse d openclassrooms ?"
        )

    def test_trigger_api_good_len_query(self):
        pass

    def test_trigger_api_short_query(self):
        """
        Cause a persistent geo_data['context'] = 'textinput too short'
        Temporary desactivate it
        """
        # self.PLACE.query = "123"
        # self.PLACE.trigger_api()
        # assert self.PLACE.geo_data['context'] == "textinput too short"
        # assert self.PLACE.article_data['context'] == "no geo_data"
        pass

    def test_get_geo_data(self, monkeypatch):
        def mock_json_oc(*param):
            with open("tests/samples/osm-oc.json", "r") as json_file:
                return json.loads(json_file.read())

        self.PLACE.query = "OpenClassRooms"
        monkeypatch.setattr("run.Place.get_json", mock_json_oc)
        self.PLACE.set_geo_data()
        assert self.PLACE.geo_data == self.OC_GEO_JSON

    def test_get_geo_data_missing_field(self, monkeypatch):
        def mock_json_missing_field(*param):
            with open("tests/samples/osm-missing-oc.json", "r") as json_file:
                return json.loads(json_file.read())

        self.PLACE.query = self.TXTINPUT
        monkeypatch.setattr("run.Place.get_json", mock_json_missing_field)
        self.PLACE.set_geo_data()
        assert self.PLACE.geo_data["error"]["KeyError"] == "display_name"

    def test_set_article_data(self, monkeypatch):
        def mock_json_oc_wiki_list(*param):
            with open("tests/samples/oc-wiki.json", "r") as json_file:
                return json.loads(json_file.read())

        self.PLACE.query = self.TXTINPUT
        monkeypatch.setattr("run.Place.get_json", mock_json_oc_wiki_list)
        self.PLACE.set_article_data()
        assert self.PLACE.article_data["status"]

    def test__google_static_valid(self):
        self.PLACE.geo_data["lat"] = "48.8747578"
        self.PLACE.geo_data["lon"] = "2.35056470"

        # Testing response URL without API key
        url = self.PLACE._google_static()

        if "key=" in url:
            old_parts = up.urlparse(url)
            padic = up.parse_qs(old_parts.query)
            del padic["key"]
            query = up.urlencode(padic, doseq=True)
            parts = (old_parts.scheme, old_parts.netloc, old_parts.path, "", query, "")
            url = up.urlunparse(parts)

        assert (
            url
            == "https://maps.googleapis.com/maps/api/staticmap?center=48.8747578%2C2.35056470&markers=48.8747578%2C2.35056470&size=600x300"
        )

    def test__mapbox_static_valid(self):
        self.PLACE.geo_data["lat"] = "48.8747578"
        self.PLACE.geo_data["lon"] = "2.35056470"

        # Get URL
        url = self.PLACE._mapbox_static()

        # Removing API key : query string
        old_parts = up.urlparse(url)
        parts = (old_parts.scheme, old_parts.netloc, old_parts.path, "", "", "")
        url = up.urlunparse(parts)

        assert (
            url
            == "https://api.mapbox.com/v4/mapbox.streets/pin-s-marker+285A98(2.35056470,48.8747578)/2.35056470,48.8747578,16/600x300.png"
        )


class RequestsResponse:
    """ Requests.reponse object mock """

    status_code = 200

    def json():
        return [11, 22, 33, 44, 55]


def mock_requests_get(url, payload):
    """ Requests.get() function mock """

    return RequestsResponse


def test_get_json_valid():
    """ Test Place.get_json() with basic manual mock """

    # backup original function
    orginal_function = models.place.requests.get

    # override function with my mock
    models.place.requests.get = mock_requests_get

    # Running the tested function, fake params due to the mock
    response = script.Place.get_json("url", "payload")

    # Test
    assert response[2] == 33

    # Rolling bak
    models.place.requests.get = orginal_function


class RequestsResponseInvalid:
    """ Requests.reponse object mock """

    status_code = 300

    def json():
        return "{'context': 'get_json() method', 'error': {'status_code': 'response.status_code'}}"


def mock_requests_get_invalid(url, payload):
    """ Requests.get() function mock """
    return RequestsResponseInvalid


def test_get_json_invalid(monkeypatch):
    """ Test Place.get_json() with monkeypatch"""
    monkeypatch.setattr("models.place.requests.get", mock_requests_get_invalid)
    response = script.Place.get_json("url", "payload")
    assert response["error"]["status_code"] == 300
