#!/usr/bin/env python3
# coding: utf8

"""
Author: [freezed](https://gitlab.com/free_zed) 2018-08-25
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of project [grandpy](https://gitlab.com/free_zed/grandpy/).
"""
from stopwords import STOPW


class Query:
    """ Class doc """

    def __init__(self, textinput):
        """ Class initialiser """

        self.stop = STOPW["main"]
        self.stop.extend(STOPW["ask"])
        self.stop.extend(STOPW["hello"])
        self.stop.extend(STOPW["key"])
        self._textinput_cf = str(textinput).casefold()

        self.in_string = str()

    def parse(self):
        """
        Function documentation
        """
        notresult = []

        for word in self._textinput_cf.split():
            # word is not in stopword list
            if word not in self.stop:

                # search for single quote or hyphen in word
                squote_idx = word.find("'")

                # word contain single quote : split it after
                if squote_idx != -1:
                    squote_idx += 1

                    if word[squote_idx:] not in self.stop:
                        notresult.append(word[squote_idx:])

                # word contains alnum
                elif word.isalnum():
                    notresult.append(word)

                # cleanning word of other non-alnum character
                else:
                    cleaned_word = str()
                    for char in word:
                        if char.isalnum() or char == "-":
                            cleaned_word += char

                    if cleaned_word not in self.stop:
                        notresult.append(cleaned_word)

        self.in_string = " ".join(notresult).strip()
