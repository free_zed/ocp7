#!/usr/bin/env python3
# coding: utf8

"""
Author: [freezed](https://gitlab.com/free_zed) 2018-08-25
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of project [grandpy](https://gitlab.com/free_zed/grandpy/).
"""
import urllib.parse as up
from pprint import pformat as pf
import requests
from config import API, API_USED, WIK_API, GOOGLE_API_KEY, MAPBOX_API_KEY


class Place:
    """
    Defines a place with the user query

    Gets geo data from Google geocode & static map API
    Gets information from Wikipedia API
    """

    def __init__(self, query):
        """
        Sets arguments
        """
        self.query = str(query)
        self.geo_data = {"status": False}
        self.article_data = {"status": False}

    def trigger_api(self):
        # Get geodata
        if len(self.query) > API["MIN_QUERY_LEN"]:
            self.set_geo_data()
        else:
            self.geo_data = {"status": False, "context": "textinput too short"}

        if self.geo_data["status"]:
            self.set_article_data()
        else:
            self.article_data["context"] = "no geo_data"

    @staticmethod
    def compare(string):
        """ Returns a comparable version of string """
        return str(string).replace("-", " ").replace("'", " ").casefold()

    @staticmethod
    def get_json(url, payload):
        """
        Request API
        """
        try:
            response = requests.get(url, payload)
        except requests.exceptions.ConnectionError as except_detail:
            return {"ConnectionError": pf(except_detail)}

        try:
            api_json = response.json()
        except Exception as detail:
            return {
                "context": "get_json() method",
                "error": {"JSONDecodeError": str(detail)},
            }
        else:
            if response.status_code == 200:
                return api_json

            else:
                return {
                    "context": "get_json() method",
                    "error": {"status_code": response.status_code},
                }

    def set_article_data(self):
        """
        Function documentation
        """
        payload = {"srsearch": self.query}
        # Adds basic API call parameters
        payload.update(**WIK_API["CALL_PARAM"])

        search_json = self.get_json(WIK_API["ROOT_URL"], payload)

        try:
            self.article_data["title"] = search_json["query"]["search"][0]["title"]
            self.article_data["pageid"] = search_json["query"]["search"][0]["pageid"]

        except KeyError as detail:
            self.article_data = {
                "status": False,
                "context": "search article",
                "error": {"KeyError": str(detail), "response": search_json},
            }

        except TypeError as detail:
            self.article_data = {
                "status": False,
                "context": "search article",
                "error": {"KeyError": str(detail), "response": search_json},
            }

        except IndexError as detail:
            self.article_data = {
                "status": False,
                "context": "search article",
                "error": {"KeyError": str(detail), "response": search_json},
            }

        else:
            self.article_data["status"] = True
            payload = {"titles": self.article_data["title"]}
            # Adds basic API call parameters
            payload.update(**WIK_API["PARAM_EXTRAC"])

            article_json = self.get_json(WIK_API["ROOT_URL"], payload)

            try:
                pageid = str(self.article_data["pageid"])
                self.article_data["extract"] = article_json["query"]["pages"][pageid][
                    "extract"
                ]

            except TypeError as detail:
                self.article_data = {
                    "status": False,
                    "context": "article extract",
                    "error": {"TypeError": str(detail), "response": article_json},
                }

            except KeyError as detail:
                self.article_data = {
                    "status": False,
                    "context": "article extract",
                    "error": {"KeyError": str(detail), "response": article_json},
                }

    def set_geo_data(self):
        """
        Calls geocode API with a string query & retrieve a Place
        Filter API's JSON to keep only useful data
        """
        # Build URL request
        payload = {key: val for key, val in API[API_USED]["GEO_PARAM"].items()}
        payload.update({API[API_USED]["QUERY_LABEL"]: self.query})

        geo_json = self.get_json(API[API_USED]["URL_GEO"], payload)

        if not geo_json:
            self.geo_data = {"status": False, "error": "no_geocode_result"}

        # One result, let's parse this JSON
        else:

            # Intermediate var for a better reading
            r_param = API[API_USED]["RESPONSE_KEYS"]

            for field in r_param:

                # Field has different sources
                if isinstance(r_param[field], list):
                    # May broke here if field has a list of sources
                    # non-nested (not in a tuple)

                    for option in r_param[field]:
                        # This implies that data are only nested 1 level deep
                        level_0 = option[0]
                        level_1 = option[1]

                        try:
                            self.geo_data[field] = geo_json[0][level_0][level_1]
                        except Exception:
                            if field not in self.geo_data:
                                self.geo_data["missing_field"] = (level_0, level_1)
                        else:
                            if "missing_field" in self.geo_data:
                                del self.geo_data["missing_field"]

                    if "missing_field" in self.geo_data:
                        self.geo_data.update(
                            {"error": {"KeyError": self.geo_data["missing_field"]}}
                        )
                        del self.geo_data["missing_field"]

                # Field has a nested source
                elif isinstance(r_param[field], tuple):
                    # This implies that data are only nested 1 level deep
                    level_0 = r_param[field][0]
                    level_1 = r_param[field][1]
                    try:
                        self.geo_data[field] = geo_json[0][level_0][level_1]
                    except Exception:
                        self.geo_data["error"]["KeyError"] = (level_0, level_1)

                else:
                    try:
                        self.geo_data[field] = geo_json[0][field]
                    except Exception:
                        self.geo_data.update({"error": {"KeyError": field}})

        # Adds locality in orginal query if missing for more appropriateness
        try:
            if self.compare(self.geo_data["city"]) not in self.compare(self.query):
                self.query = "{} {}".format(self.query, self.geo_data["city"])

        except KeyError:
            self.geo_data["query_update_exception"] = "city"

        if "error" not in self.geo_data:
            self.geo_data["status"] = True

    def _google_static(self):
        """
        Return url of a static maps using Google Static Map API
        """
        coord = "{},{}".format(self.geo_data["lat"], self.geo_data["lon"])

        payload = {
            "key": GOOGLE_API_KEY,
            "center": coord,
            "markers": coord,
            "size": "{}x{}".format(*API["MAP_SIZE"]),
        }
        goo_url = up.urlparse(API["GOO"]["URL_MAP"])
        query = up.urlencode(payload)
        parts = (goo_url.scheme, goo_url.netloc, goo_url.path, "", query, "")

        return up.urlunparse(parts)

    def _mapbox_static(self):
        """
        Return url of a static maps using MapBox Static Map API
        """
        # Build URL request
        payload = {"access_token": MAPBOX_API_KEY}
        root_url = up.urlparse(
            API["OSM"]["URL_MAP"].format(
                h=API["MAP_SIZE"][0],
                w=API["MAP_SIZE"][1],
                k=MAPBOX_API_KEY,
                lat=self.geo_data["lat"],
                lon=self.geo_data["lon"],
            )
        )
        query = up.urlencode(payload)
        parts = (root_url.scheme, root_url.netloc, root_url.path, "", query, "")

        return up.urlunparse(parts)

    def get_map_src(self):
        """
        Return url of a static maps using a Static Maps API
        """
        if API_USED == "GOO":
            return self._google_static()

        elif API_USED == "OSM":
            return self._mapbox_static()
